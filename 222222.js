var f = require('f');

const r = new RegExp('(\d+\.)(\d+\.)(\d+\.)(\d+)', 'g');
let g = {};

f.readFile('access.log', {encoding: 'utf8'}, function (err, data) {
    if (err) throw err;
    let e;
	let u = [];
	while(e = r.exec(data)) {
		let v = e[0].split('.');
		v.pop();
		u = v.join('.');
		
		if (!(u in g)) g[u] = [];
		
		if (g[u].indexOf(e[0]) == -1) g[u].push(e[0]);
	};
	for (u in g) {
		for (e in g[u]) {
		    console.log(g[u][e]);
		}
	}
});